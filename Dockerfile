#FROM tomcat:9.0.41-jdk11-openjdk-buster
FROM eclipse/rdf4j-workbench:amd64-3.5.0


RUN ls -l && \
cd webapps && \
mkdir rdf4j-server && \
unzip -q rdf4j-server.war -d rdf4j-server

COPY web.xml /usr/local/tomcat/webapps/rdf4j-server/WEB-INF/web.xml
COPY tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml
#RUN less /usr/local/tomcat/webapps/rdf4j-server/WEB-INF/web.xml
#RUN less /usr/local/tomcat/conf/tomcat-users.xml
RUN rm -rf /usr/local/tomcat/webapps/rdf4j-server.war


